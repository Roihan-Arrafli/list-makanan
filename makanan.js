const listMakanan = [
    {
        namamakanan: "Rendang",
        berbahanbaku: "Daging"
    },
    {
        namamakanan: "Opor Ayam",
        berbahanbaku: "Daging"
    },
    {
        namamakanan: "Sate Kambing",
        berbahanbaku: "Daging"
    },
    {
        namamakanan: "Gudeg",
        berbahanbaku: "Sayuran"
    },
    {
        namamakanan: "Sayur Lodeh",
        berbahanbaku: "Sayuran"
    },
    {
        namamakanan: "Sayur Asam",
        berbahanbaku: "Sayuran"
    }
];

for(let i = 0; i<listMakanan.length; i++){
    console.log(listMakanan[i]["namamakanan"], 
    ("berbahan baku dari " + listMakanan[i]["berbahanbaku"]));
}

console.log("-cari-")
function cariMakanan(namamakanan) {
    for(let i = 0; i <listMakanan.length; i++){
        if (listMakanan[i]["namamakanan"] == namamakanan) {
            console.log(listMakanan[i]["namamakanan"], 
            ("berbahan baku dari " + 
            listMakanan[i]["berbahanbaku"]));
        }
    }
}
cariMakanan("Rendang");

console.log("-filter-")
function filterMakanan(berbahanbaku) {
    for(let i = 0; i < listMakanan.length; i++){
        if (listMakanan[i]["berbahanbaku"] == berbahanbaku) {
            console.log(listMakanan[i]["namamakanan"]);
        }
    }
}
filterMakanan("Sayuran")